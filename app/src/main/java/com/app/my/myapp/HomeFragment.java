package com.app.my.myapp;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.view.ViewPager;
import android.support.design.widget.TabLayout;
import android.widget.LinearLayout;

public class HomeFragment extends Fragment {

    //Screen Navigation listener
    private ScreenNavigation mScreenNavigation = null;

    private View rootView;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_fragment, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Add tabs icon with setIcon() or simple text with .setText()
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.home_icon));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.profile_icon));

        // Set divider tabs
        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.white));
            drawable.setSize(2, 1);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }

        //Add fragments
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        ListFragment listFragment = new ListFragment();
        listFragment.setNavigationListener(getScreenNavigationListener());
        adapter.addFragment(listFragment);
        adapter.addFragment(new ProfileFragment());

        //Setting adapter
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

    }

    private ScreenNavigation getScreenNavigationListener() {
        return mScreenNavigation;
    }

    /**
     * Set the screen navigation instance
     * @param aScreenNavigation ScreenNavigation Listener
     */
    public void setNavigationListener(ScreenNavigation aScreenNavigation){
        mScreenNavigation = aScreenNavigation;
    }
}
