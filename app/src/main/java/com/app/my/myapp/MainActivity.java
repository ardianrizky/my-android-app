package com.app.my.myapp;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;

public class MainActivity extends AppCompatActivity implements ScreenNavigation, FragmentManager.OnBackStackChangedListener{

    public enum FragmentType {
        LOGIN,
        HOME,
        DETAIL
    }

    /**
     * Current fragment type shown in the main view.
     */
    private FragmentType mCurrentFragmentType = FragmentType.HOME;

    private LoginFragment loginFragment;
    private HomeFragment homeFragment;
    private DetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        boolean isLogin = sharedPref.getBoolean(getString(R.string.is_login), false);

        if (isLogin) {
            openHomeFragment();
        } else {
            openLoginFragment();
        }
    }

    private void hideHeader(boolean isHide) {
        if (isHide) {
            getSupportActionBar().hide();
        } else {
            getSupportActionBar().show();
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar_view);
        }
    }

    private void openHomeFragment() {
        mCurrentFragmentType = FragmentType.HOME;
        hideHeader(false);
        homeFragment = getHomeFragment();
        if (homeFragment == null) {
            homeFragment = new HomeFragment();
        }

        homeFragment.setNavigationListener(this);
        makeFragmentTransaction(homeFragment, true);
    }

    private void openLoginFragment() {
        mCurrentFragmentType = FragmentType.LOGIN;
        hideHeader(true);
        loginFragment = getLoginFragment();
        if (loginFragment == null) {
            loginFragment = new LoginFragment();
        }

        loginFragment.setNavigationListener(this);
        makeFragmentTransaction(loginFragment, false);
    }

    private void openDetailFragment() {
        mCurrentFragmentType = FragmentType.DETAIL;
        hideHeader(false);
        detailFragment = getDetailFragment();
        if (detailFragment == null) {
            detailFragment = new DetailFragment();
        }

        detailFragment.setNavigationListener(this);
        makeFragmentTransaction(detailFragment, true);
    }

    /**
     * Returns the Login Screen Fragment
     *
     * @return - instance of discover fragment
     */
    private LoginFragment getLoginFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(mCurrentFragmentType.name());
        if (fragment != null && (fragment instanceof LoginFragment)) {
            return (LoginFragment) fragment;
        }
        return null;
    }

    /**
     * Returns the Home Screen Fragment
     *
     * @return - instance of discover fragment
     */
    private HomeFragment getHomeFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(mCurrentFragmentType.name());
        if (fragment != null && (fragment instanceof HomeFragment)) {
            return (HomeFragment) fragment;
        }
        return null;
    }

    /**
     * Returns the Detail Screen Fragment
     *
     * @return - instance of discover fragment
     */
    private DetailFragment getDetailFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(mCurrentFragmentType.name());
        if (fragment != null && (fragment instanceof DetailFragment)) {
            return (DetailFragment) fragment;
        }
        return null;
    }

    /**
     * Make fragment transaction
     *
     * @param aFragment - fragment name
     */
    private void makeFragmentTransaction(Fragment aFragment, boolean shouldAddBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, aFragment,
                mCurrentFragmentType.name());
        if(shouldAddBackStack){
            fragmentTransaction.addToBackStack(mCurrentFragmentType.name());
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onScreenNavigation(FragmentNavigation fragmentNavigation, Bundle bundle) {
        switch (fragmentNavigation) {
            case HOME:
                openHomeFragment();
                break;
            case DETAIL:
                openDetailFragment();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackStackChanged() {
        // Select the appropriate navigation drawer item based on the type of fragment opened.
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final int backStackEntryCount = fragmentManager.getBackStackEntryCount();

        // If their is no entry left in the back stack then set the current fragment as discover.
        if (backStackEntryCount == 0) {
            mCurrentFragmentType = FragmentType.HOME;
        } else {
            final FragmentManager.BackStackEntry backStackEntry =
                    fragmentManager.getBackStackEntryAt(backStackEntryCount - 1);
            final String backStackEntryName = backStackEntry.getName();
            mCurrentFragmentType = FragmentType.valueOf(backStackEntryName);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // handle back button from home page
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        if (backStackEntryCount > 0)
            getFragmentManager().popBackStackImmediate();
        else {
            finish();
        }
    }
}
