package com.app.my.myapp;

public enum FragmentNavigation {
    // Login Screen
    LOGIN,
    // Home Screen
    HOME,
    // Detail Screen
    DETAIL
}
