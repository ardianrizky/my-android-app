package com.app.my.myapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment {

    //Screen Navigation listener
    private ScreenNavigation mScreenNavigation = null;

    private View rootView;
    ImageView image;

    Integer[] images={
            R.drawable.image1,
            R.drawable.image2,
            R.drawable.image3
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.list_fragment, container, false);
        image = (ImageView) rootView.findViewById(R.id.image);

        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
        for(int i=0; i<images.length ;i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("image", Integer.toString(images[i]) );
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = { "image" };

        // Ids of views in listview_layout
        int[] to = { R.id.image };

        SimpleAdapter adapter = new SimpleAdapter(rootView.getContext(), aList, R.layout.image_list, from, to);

        ListView list = ( ListView ) rootView.findViewById(R.id.images);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                goToDetailPage();
            }
        });
        list.setAdapter(adapter);
        return rootView;
    }

    /**
     * Set the screen navigation instance
     * @param aScreenNavigation ScreenNavigation Listener
     */
    public void setNavigationListener(ScreenNavigation aScreenNavigation){
        mScreenNavigation = aScreenNavigation;
    }

    private void goToDetailPage() {
        sendNavigationData(FragmentNavigation.DETAIL, null);
    }

    private void sendNavigationData(FragmentNavigation fragmentNavigation, Bundle bundle) {
        if(mScreenNavigation != null){
            mScreenNavigation.onScreenNavigation(fragmentNavigation, bundle);
        }
    }
}
