package com.app.my.myapp;

import android.support.v4.app.Fragment;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.content.Context;
import android.content.SharedPreferences;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

public class LoginFragment extends Fragment implements View.OnClickListener {

    //Screen Navigation listener
    private ScreenNavigation mScreenNavigation = null;

    private EditText email, password;
    private Button loginBtn;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.login_fragment, container, false);
        email = (EditText)rootView.findViewById(R.id.EmailEditText);
        password = (EditText)rootView.findViewById(R.id.PasswordEditText);
        loginBtn = (Button)rootView.findViewById(R.id.LoginButton);
        loginBtn.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.LoginButton:
                doLogin();
                break;
            default:
                break;
        }
    }

    private void doLogin() {
        String emailVal = email.getText().toString().toLowerCase();
        String passwordVal = password.getText().toString();
        if (emailVal != null && !emailVal.equalsIgnoreCase("") && passwordVal != null && !passwordVal.equalsIgnoreCase("")) {

            // Set session to local storage
            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean("IS_LOGIN", true);
            editor.commit();

            hideKeyboard();

            sendNavigationData(FragmentNavigation.HOME, null);
        } else {
            CharSequence text = "Email & password tidak boleh kosong!";
            int duration = Toast.LENGTH_SHORT;
            Toast.makeText(getActivity(), text, duration).show();
        }
    }

    /**
     * Hide virtual keyboard
     */
    private void hideKeyboard() {
        if (getActivity() != null) {
            InputMethodManager keyboard = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(email.getWindowToken(), 0);
            keyboard.hideSoftInputFromWindow(password.getWindowToken(), 0);
        }
    }

    private void sendNavigationData(FragmentNavigation fragmentNavigation, Bundle bundle) {
        if(mScreenNavigation != null){
            mScreenNavigation.onScreenNavigation(fragmentNavigation, bundle);
        }
    }

    /**
     * Set the screen navigation instance
     * @param aScreenNavigation ScreenNavigation Listener
     */
    public void setNavigationListener(ScreenNavigation aScreenNavigation){
        mScreenNavigation = aScreenNavigation;
    }
}
