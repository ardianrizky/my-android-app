package com.app.my.myapp;

import android.os.Bundle;

public interface ScreenNavigation {
    void onScreenNavigation(FragmentNavigation fragmentNavigation, Bundle bundle);
}
