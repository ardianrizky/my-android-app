package com.app.my.myapp;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class DetailFragment extends Fragment {

    private View rootView;
    //Screen Navigation listener
    private ScreenNavigation mScreenNavigation = null;
    CarouselView carouselView;
    int[] sampleImages = {R.drawable.sample1, R.drawable.sample2, R.drawable.sample3};

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.detail_fragment, container, false);

        carouselView = rootView.findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);
        carouselView.setImageListener(imageListener);
        return rootView;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };


    /**
     * Set the screen navigation instance
     * @param aScreenNavigation ScreenNavigation Listener
     */
    public void setNavigationListener(ScreenNavigation aScreenNavigation){
        mScreenNavigation = aScreenNavigation;
    }
}
